# EDOM - Project Research Topic - Graphical Concrete Syntaxes (Using Microsoft DSL SDK)  

Authors:
- 1150547: Diogo Araújo
- 1150690: Sofia Silva
- 1150780: Tiago Leite
- 1151421: Francisco Ferraz

December 2018
EDOM 

Professors:
- Alexandre Bragança, ATB
- Sofia Azevedo, SFA

## Contents

1. [Introduction](#introduction)  
2. [Graphical Concrete Sintaxes](#graphical-concrete-sintaxes)
3. [Modeling SDK for Visual Studio](#modeling-sdk-for-visual-studio)
1. [Xtext vs Microsoft DSL SDK](#xtext-vs-microsoft-dsl-sdk)
1. [References](#references)

## Introduction 
This research was carried out within the domain engineering course, aiming to acquire knowledge about new tools of a certain topic. This document covers topics such as concrete graphical syntax, the use of the Microsoft DSL SDK to generate Domain-Specific Languages ​​and their comparison with the Xtext framework.


## Graphical Concrete Sintaxes
The definition of a DSL's (Domain-Specific Language) involves different kinds of activities, being the creation of the syntax one of them. A DSL's syntax is specified in two parts: the abstract syntax defines the language's concepts and their allowed combinations; the concrete syntax defines how those concepts are presented to the user.  
The concrete syntax of a language is either textual or graphical and is what the designers looks up as reference in modeling activities.  
The graphical concrete syntax is defined by the structure of the abstract syntax and a set of graphical representations for classes and associations in the abstract syntax.  For languages that do not have an adequate graphical representation, normally a textual syntax is used which is usually described by a context-free grammar [\[1\]](#reference).

In this research we will define a DSL using MSDK (with Graphical Concrete Syntaxes) and compare with the approach used in classes (XText) and verify its pros and cons.

## Modeling SDK for Visual Studio 

With Visual Studio, it's possible to create powerful model-based development tools, using Modeling SDK.
This tool surround the model with a diagram Views, the ability to generate code, commands for transforming the model and the hability to interact with code and other objects in Visual Studio [\[2\]](#references), it's represented on figure 1.

| ![Figure 1](./images/figure1.png) | 
|:--:| 
| Figure 1 - View of MSDK for Visual Studio, Source: [\[2\]](#references) |

The MSDK (Modeling Software Development Kit) allows to develop a model quickly in the form of a DSL (Domain-Specific Language)  [\[2\]](#references).

To define a DSL, the following components must be installed:
* Visual Studio
* Visual Studio SDK
* Modeling SDK ( In Visual Studio 2017, the component is automatically installed as part of the Visual Studio extension devlopment)
* Text Template Transformation (Also, in Visual Studio 2017, is installed as part of the Visual Studio extension Devlopment)


### Create a DSL Solution

To create a new Domain-Specific language, first it's required to use a Visual Studio Solution, using a Domain-Specific Language Project Template.

In order to create a new one, the steps should be:

1. On the File menu, point to New, and then click Project.  
2. Under Project types, expand the Other Project Types node, and click Extensibility.  
3. Click Domain-Specific Language Designer.  

| ![Figure 2](./images/figure2.png) | 
|:--:| 
| Figure 2 - Create a DSL Solution, Source:[\[2\]](#references) |

4. Insert the name on Name box and click OK.

The Domain-Specific Language Wizard opens, and displays a list of template DSL solutions.

### The important parts of the DSL Solution

* DSL\DslDefinition.dsl : All the elements and relationships of the model are define in this file, on a diagram. Almost all the code in the solution are generated from this file. 
* DSL Project : This project contains code that defines the Domain-Specific Language.
* DslPackage project: this project contains code that allows instances of the DSL to be opened and edited in Visual Studio.

### Running the DSL 

1. In the solution toolbar, click  Transform All Templates. This funcionality regerates most of the source code from DslDefinition.dsl  
2. Press F5 to start Debugging or on Debug menu, click Start Debugging.  
    The DSL builds and is installed in the experimental instance of Visual Studio.  
3. In the experimental instance of Visual Studio, open the model file named Test from Solution Explorer.  
4. Use the tools to create shapes and connectors on the diagram.  

| ![Figure 3](./images/figure3.png) | 
|:--:| 
| Figure 3 - Running DSL, Source: [\[3\]](#references) |

## Xtext vs Modeling SDK  
When talking about modeling, most of us instinctively remember creating graphical class diagrams, as is the case with the tool Modeling SDK. In oppsosition to this approach, in the course of Domain Engineering, Xtext was used to implement textual concrete syntaxes for existing EMF metamodels. Xtext is a framework for building language workbenches for textual domain-specific languages [\[4\]](#references). The Figure 4 shows an example of the grammar MyDsl.

| ![Figure 4](./images/xtext_ex.PNG) | 
|:--:| 
| Figure 4 - Example of a DSL, Source: [\[5\]](#references) |

The text files created using the Xtext editor, are analyzed by a parser, that instantiates an Ecore model representing the abstract syntax tree. Xtext languages can be integrated into different IDEs such as the Eclipse, IntelliJ, Visual Studio Code, and many others [\[6\]](#references). This can be considered an advantage, since it is not dependent on just one technology. On the contrary, when using Modeling SDK, only Visual Studio can be used.  

All the aspects of a DSL implemented in Xtext can be implemented in Xtend instead of Java. Xtend is implemented in Xtext and it is easier to use, allowing to write better readable code. In Modeling SDK, template files (.tt) are generated with notations to process the DSL. This one are implemented in CSharp.  

Regarding the maintainability of the models, it is easier to create and maintain models via text files, as is the case of Xtext. However it is beneficial to have a graphical representation to discuss the broader domain concepts and their relations, like Microsoft DSL SDK.  


# References

[1]: Sintef.no. (2018). Model-Driven Analysis and Synthesis of Textual Concrete Syntax. [online] Available at: https://www.sintef.no/globalassets/upload/ikt/9012/mdasocs-sosym.pdf [Accessed 1 Dec. 2018].

[2]: Contributors (2016, April 11). Modeling SDK for Visual Studio - Domain-Specific Languages Retrived from https://docs.microsoft.com/en-us/visualstudio/modeling/modeling-sdk-for-visual-studio-domain-specific-languages?view=vs-2017

[3]: Contributors (2016, April 11). Get Started with Domain-Specific Languages. Retrieved from https://docs.microsoft.com/en-us/visualstudio/modeling/getting-started-with-domain-specific-languages?view=vs-2017

[4]: B�nder, Hendrik (2018, March 15). Building Domain-specific Languages with Xtext and Xtend. Retrieved from https://blogs.itemis.com/en/building-domain-specific-languages-with-xtext-and-xtend?fbclid=IwAR1KTR4vNPtffL-uJELwIgfs88SvyKQBY1Ed4F0iiEY7GnHWR8hXcUWDsS8

[5]: B�nder, Hendrik (2018, Mar 15). Building Domain-specific Languages with Xtext and Xtend. Retrieved from https://blogs.itemis.com/en/building-domain-specific-languages-with-xtext-and-xtend?fbclid=IwAR1KTR4vNPtffL-uJELwIgfs88SvyKQBY1Ed4F0iiEY7GnHWR8hXcUWDsS8

[6]: Eclipse (2013, December 16), Xtext 2.5 Documentation
