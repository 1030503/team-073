﻿<?xml version="1.0" encoding="utf-8"?>
<Dsl xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="0d07933c-7007-40fe-b015-1fb99ac4a85a" Description="Description for edom.requirements.requirements" Name="requirements" DisplayName="requirements" Namespace="edom.requirements" ProductName="requirements" CompanyName="edom" PackageGuid="a10b8107-aa1a-4259-aca5-44af92603100" PackageNamespace="edom.requirements" xmlns="http://schemas.microsoft.com/VisualStudio/2005/DslTools/DslDefinitionModel">
  <Classes>
    <DomainClass Id="f4f5bfbe-567e-4593-a215-3a863a719aac" Description="The root in which all other elements are embedded. Appears as a diagram." Name="Model" DisplayName="Model" Namespace="edom.requirements">
      <Properties>
        <DomainProperty Id="f39ca8c9-baaa-499e-ae82-ff7e007abc20" Description="Description for edom.requirements.Model.Title" Name="title" DisplayName="Title">
          <Type>
            <ExternalTypeMoniker Name="/System/String" />
          </Type>
        </DomainProperty>
      </Properties>
      <ElementMergeDirectives>
        <ElementMergeDirective>
          <Notes>Creates an embedding link when an element is dropped onto a model. </Notes>
          <Index>
            <DomainClassMoniker Name="RequirementGroup" />
          </Index>
          <LinkCreationPaths>
            <DomainPath>ModelHasGroups.Groups</DomainPath>
          </LinkCreationPaths>
        </ElementMergeDirective>
      </ElementMergeDirectives>
    </DomainClass>
    <DomainClass Id="128dd1c5-d8ad-49e2-a9f8-71fc6ff759c1" Description="Elements embedded in the model. Appear as boxes on the diagram." Name="RequirementGroup" DisplayName="Requirement Group" Namespace="edom.requirements">
      <Properties>
        <DomainProperty Id="edbe4e8d-2391-4bc5-a684-c7e9decbabe4" Description="Description for edom.requirements.RequirementGroup.Name" Name="name" DisplayName="Name" DefaultValue="" IsElementName="true">
          <Type>
            <ExternalTypeMoniker Name="/System/String" />
          </Type>
        </DomainProperty>
        <DomainProperty Id="a4e1a2b8-1a91-446d-8f01-b6ea65ca4aa3" Description="Description for edom.requirements.RequirementGroup.Description" Name="description" DisplayName="Description">
          <Type>
            <ExternalTypeMoniker Name="/System/String" />
          </Type>
        </DomainProperty>
        <DomainProperty Id="398a7c05-04ac-4719-92e3-40af540276a5" Description="Description for edom.requirements.RequirementGroup.Id" Name="id" DisplayName="Id">
          <Type>
            <ExternalTypeMoniker Name="/System/String" />
          </Type>
        </DomainProperty>
      </Properties>
      <ElementMergeDirectives>
        <ElementMergeDirective>
          <Index>
            <DomainClassMoniker Name="Requirement" />
          </Index>
          <LinkCreationPaths>
            <DomainPath>RequirementGroupHasRequirements.Requirements</DomainPath>
          </LinkCreationPaths>
        </ElementMergeDirective>
      </ElementMergeDirectives>
    </DomainClass>
    <DomainClass Id="8b0d76c3-4cf5-40e9-b2fa-7ac19747ad08" Description="Description for edom.requirements.Requirement" Name="Requirement" DisplayName="Requirement" Namespace="edom.requirements">
      <Properties>
        <DomainProperty Id="7917dd6b-91ae-42ce-8f5b-7bd50d77dd32" Description="Description for edom.requirements.Requirement.Title" Name="title" DisplayName="Title">
          <Type>
            <ExternalTypeMoniker Name="/System/String" />
          </Type>
        </DomainProperty>
        <DomainProperty Id="d70e07b0-0dca-4180-939c-0f0992963fb7" Description="Description for edom.requirements.Requirement.Description" Name="description" DisplayName="Description">
          <Type>
            <ExternalTypeMoniker Name="/System/String" />
          </Type>
        </DomainProperty>
        <DomainProperty Id="516167c3-23ba-4a3b-967c-ed27bd8649b7" Description="Description for edom.requirements.Requirement.Id" Name="id" DisplayName="Id">
          <Type>
            <ExternalTypeMoniker Name="/System/String" />
          </Type>
        </DomainProperty>
      </Properties>
      <ElementMergeDirectives>
        <ElementMergeDirective>
          <Index>
            <DomainClassMoniker Name="Comment" />
          </Index>
          <LinkCreationPaths>
            <DomainPath>RequirementHasComments.Comments</DomainPath>
          </LinkCreationPaths>
        </ElementMergeDirective>
        <ElementMergeDirective>
          <Index>
            <DomainClassMoniker Name="Version" />
          </Index>
          <LinkCreationPaths>
            <DomainPath>RequirementHasVersion.Version</DomainPath>
          </LinkCreationPaths>
        </ElementMergeDirective>
      </ElementMergeDirectives>
    </DomainClass>
    <DomainClass Id="87677480-3bc4-4e80-861a-375aa4f1e5df" Description="Description for edom.requirements.Comment" Name="Comment" DisplayName="Comment" Namespace="edom.requirements">
      <Properties>
        <DomainProperty Id="79ea5a25-c547-4781-a935-db29fcab3beb" Description="Description for edom.requirements.Comment.Subject" Name="subject" DisplayName="Subject">
          <Type>
            <ExternalTypeMoniker Name="/System/String" />
          </Type>
        </DomainProperty>
        <DomainProperty Id="ba774ec0-bbb4-4d14-890d-31e0f284a43c" Description="Description for edom.requirements.Comment.Body" Name="body" DisplayName="Body">
          <Type>
            <ExternalTypeMoniker Name="/System/String" />
          </Type>
        </DomainProperty>
        <DomainProperty Id="4700eba2-c9d5-475c-b278-b30d715fa8cf" Description="Description for edom.requirements.Comment.Author" Name="author" DisplayName="Author">
          <Type>
            <ExternalTypeMoniker Name="/System/String" />
          </Type>
        </DomainProperty>
        <DomainProperty Id="82d7f7cf-57ec-40f4-99ea-c67acfde7291" Description="Description for edom.requirements.Comment.Created" Name="created" DisplayName="Created">
          <Type>
            <ExternalTypeMoniker Name="/System/String" />
          </Type>
        </DomainProperty>
      </Properties>
    </DomainClass>
    <DomainClass Id="01f31e06-b5f8-437b-bc6d-ea8b33cb726c" Description="Description for edom.requirements.Version" Name="Version" DisplayName="Version" Namespace="edom.requirements">
      <Properties>
        <DomainProperty Id="6ef7582d-6520-4675-b672-c6f690ed7da4" Description="Description for edom.requirements.Version.Major" Name="major" DisplayName="Major">
          <Type>
            <ExternalTypeMoniker Name="/System/String" />
          </Type>
        </DomainProperty>
        <DomainProperty Id="4a78d62a-5fef-4642-9625-003ffb8a2ee6" Description="Description for edom.requirements.Version.Minor" Name="minor" DisplayName="Minor">
          <Type>
            <ExternalTypeMoniker Name="/System/String" />
          </Type>
        </DomainProperty>
        <DomainProperty Id="36feafdc-3520-4605-80d5-8250580283b8" Description="Description for edom.requirements.Version.Service" Name="service" DisplayName="Service">
          <Type>
            <ExternalTypeMoniker Name="/System/String" />
          </Type>
        </DomainProperty>
      </Properties>
    </DomainClass>
  </Classes>
  <Relationships>
    <DomainRelationship Id="044d6028-93a2-4675-bb45-8ecf9a1e5989" Description="Embedding relationship between the Model and Elements" Name="ModelHasGroups" DisplayName="Model Has Groups" Namespace="edom.requirements" IsEmbedding="true">
      <Source>
        <DomainRole Id="5a0e12f3-b3bc-42d9-9e3f-d349b3b11cf8" Description="" Name="Model" DisplayName="Model" PropertyName="Groups" PropagatesCopy="PropagatesCopyToLinkAndOppositeRolePlayer" PropertyDisplayName="Groups">
          <RolePlayer>
            <DomainClassMoniker Name="Model" />
          </RolePlayer>
        </DomainRole>
      </Source>
      <Target>
        <DomainRole Id="36ef22d8-16ff-4c59-a254-0b4541c01eae" Description="" Name="Element" DisplayName="Element" PropertyName="Model" Multiplicity="One" PropagatesDelete="true" PropertyDisplayName="Model">
          <RolePlayer>
            <DomainClassMoniker Name="RequirementGroup" />
          </RolePlayer>
        </DomainRole>
      </Target>
    </DomainRelationship>
    <DomainRelationship Id="4c5609f5-e104-438b-8b4c-6e133a3fbb63" Description="Description for edom.requirements.RequirementGroupHasRequirements" Name="RequirementGroupHasRequirements" DisplayName="Requirement Group Has Requirements" Namespace="edom.requirements" IsEmbedding="true">
      <Source>
        <DomainRole Id="be4558d4-3ee5-413e-85d5-c34a15bb616d" Description="Description for edom.requirements.RequirementGroupHasRequirements.RequirementGroup" Name="RequirementGroup" DisplayName="Requirement Group" PropertyName="Requirements" PropagatesCopy="PropagatesCopyToLinkAndOppositeRolePlayer" PropertyDisplayName="Requirements">
          <RolePlayer>
            <DomainClassMoniker Name="RequirementGroup" />
          </RolePlayer>
        </DomainRole>
      </Source>
      <Target>
        <DomainRole Id="eb068b2b-96c9-45d0-8d17-73c49cec1f4c" Description="Description for edom.requirements.RequirementGroupHasRequirements.Requirement" Name="Requirement" DisplayName="Requirement" PropertyName="RequirementGroup" Multiplicity="One" PropagatesDelete="true" PropertyDisplayName="Requirement Group">
          <RolePlayer>
            <DomainClassMoniker Name="Requirement" />
          </RolePlayer>
        </DomainRole>
      </Target>
    </DomainRelationship>
    <DomainRelationship Id="cef60cd4-298c-47be-bbb3-cad79dbc07d9" Description="Description for edom.requirements.RequirementHasComments" Name="RequirementHasComments" DisplayName="Requirement Has Comments" Namespace="edom.requirements" IsEmbedding="true">
      <Source>
        <DomainRole Id="3d61ce67-2eb4-4ffb-b51f-68c658717f40" Description="Description for edom.requirements.RequirementHasComments.Requirement" Name="Requirement" DisplayName="Requirement" PropertyName="Comments" PropagatesCopy="PropagatesCopyToLinkAndOppositeRolePlayer" PropertyDisplayName="Comments">
          <RolePlayer>
            <DomainClassMoniker Name="Requirement" />
          </RolePlayer>
        </DomainRole>
      </Source>
      <Target>
        <DomainRole Id="9f12cf24-2010-473f-8680-5f8198a5e38c" Description="Description for edom.requirements.RequirementHasComments.Comment" Name="Comment" DisplayName="Comment" PropertyName="Requirement" Multiplicity="One" PropagatesDelete="true" PropertyDisplayName="Requirement">
          <RolePlayer>
            <DomainClassMoniker Name="Comment" />
          </RolePlayer>
        </DomainRole>
      </Target>
    </DomainRelationship>
    <DomainRelationship Id="2ca49360-2441-4d7e-a95d-01cef9229100" Description="Description for edom.requirements.RequirementHasVersion" Name="RequirementHasVersion" DisplayName="Requirement Has Version" Namespace="edom.requirements" IsEmbedding="true">
      <Source>
        <DomainRole Id="6ad981bb-f13a-4d6c-b384-0cb3434aa995" Description="Description for edom.requirements.RequirementHasVersion.Requirement" Name="Requirement" DisplayName="Requirement" PropertyName="Version" Multiplicity="One" PropagatesCopy="PropagatesCopyToLinkAndOppositeRolePlayer" PropertyDisplayName="Version">
          <RolePlayer>
            <DomainClassMoniker Name="Requirement" />
          </RolePlayer>
        </DomainRole>
      </Source>
      <Target>
        <DomainRole Id="ba607fdc-db41-4108-a85d-5f8d665667e4" Description="Description for edom.requirements.RequirementHasVersion.Version" Name="Version" DisplayName="Version" PropertyName="Requirement" Multiplicity="One" PropagatesDelete="true" PropertyDisplayName="Requirement">
          <RolePlayer>
            <DomainClassMoniker Name="Version" />
          </RolePlayer>
        </DomainRole>
      </Target>
    </DomainRelationship>
  </Relationships>
  <Types>
    <ExternalType Name="DateTime" Namespace="System" />
    <ExternalType Name="String" Namespace="System" />
    <ExternalType Name="Int16" Namespace="System" />
    <ExternalType Name="Int32" Namespace="System" />
    <ExternalType Name="Int64" Namespace="System" />
    <ExternalType Name="UInt16" Namespace="System" />
    <ExternalType Name="UInt32" Namespace="System" />
    <ExternalType Name="UInt64" Namespace="System" />
    <ExternalType Name="SByte" Namespace="System" />
    <ExternalType Name="Byte" Namespace="System" />
    <ExternalType Name="Double" Namespace="System" />
    <ExternalType Name="Single" Namespace="System" />
    <ExternalType Name="Guid" Namespace="System" />
    <ExternalType Name="Boolean" Namespace="System" />
    <ExternalType Name="Char" Namespace="System" />
  </Types>
  <Shapes>
    <GeometryShape Id="45194d2c-ad9c-44df-af5f-9f52b8815105" Description="Description for edom.requirements.RequirementGroupShape" Name="RequirementGroupShape" DisplayName="Requirement Group Shape" Namespace="edom.requirements" FixedTooltipText="Requirement Group Shape" FillColor="255, 192, 128" InitialHeight="1" Geometry="RoundedRectangle">
      <ShapeHasDecorators Position="InnerTopLeft" HorizontalOffset="0" VerticalOffset="0">
        <TextDecorator Name="name" DisplayName="Name" DefaultText="name" />
      </ShapeHasDecorators>
    </GeometryShape>
    <GeometryShape Id="72f7a86b-e83f-4476-9032-89143b9a76d1" Description="Description for edom.requirements.RequirementShape" Name="RequirementShape" DisplayName="Requirement Shape" Namespace="edom.requirements" FixedTooltipText="Requirement Shape" FillColor="255, 255, 128" InitialHeight="1" Geometry="Ellipse">
      <ShapeHasDecorators Position="InnerTopLeft" HorizontalOffset="0" VerticalOffset="0">
        <TextDecorator Name="title" DisplayName="Title" DefaultText="title" />
      </ShapeHasDecorators>
    </GeometryShape>
    <GeometryShape Id="4b6cd477-e398-41e0-a536-f3d5ed9ed447" Description="Description for edom.requirements.CommentShape" Name="CommentShape" DisplayName="Comment Shape" Namespace="edom.requirements" FixedTooltipText="Comment Shape" FillColor="224, 224, 224" OutlineColor="Gray" InitialHeight="1" FillGradientMode="ForwardDiagonal" Geometry="Rectangle">
      <ShapeHasDecorators Position="InnerTopLeft" HorizontalOffset="0" VerticalOffset="0">
        <TextDecorator Name="subject" DisplayName="Subject" DefaultText="subject" />
      </ShapeHasDecorators>
    </GeometryShape>
    <GeometryShape Id="08254b76-86c1-458b-ab46-160351b9b87e" Description="Description for edom.requirements.VersionShape" Name="VersionShape" DisplayName="Version Shape" Namespace="edom.requirements" FixedTooltipText="Version Shape" FillColor="192, 255, 255" InitialHeight="1" FillGradientMode="None" Geometry="Rectangle">
      <ShapeHasDecorators Position="InnerTopLeft" HorizontalOffset="0" VerticalOffset="0">
        <TextDecorator Name="major" DisplayName="Major" DefaultText="major" />
      </ShapeHasDecorators>
      <ShapeHasDecorators Position="InnerTopLeft" HorizontalOffset="0" VerticalOffset="0">
        <TextDecorator Name="minor" DisplayName="Minor" DefaultText="minor" />
      </ShapeHasDecorators>
    </GeometryShape>
  </Shapes>
  <Connectors>
    <Connector Id="09480a68-8dfe-4eea-8f28-2236c8ebe612" Description="Description for edom.requirements.Connector" Name="Connector" DisplayName="Connector" Namespace="edom.requirements" FixedTooltipText="Connector" Color="Gray" />
  </Connectors>
  <XmlSerializationBehavior Name="requirementsSerializationBehavior" Namespace="edom.requirements">
    <ClassData>
      <XmlClassData TypeName="Model" MonikerAttributeName="" SerializeId="true" MonikerElementName="modelMoniker" ElementName="model" MonikerTypeName="ModelMoniker">
        <DomainClassMoniker Name="Model" />
        <ElementData>
          <XmlRelationshipData RoleElementName="groups">
            <DomainRelationshipMoniker Name="ModelHasGroups" />
          </XmlRelationshipData>
          <XmlPropertyData XmlName="title">
            <DomainPropertyMoniker Name="Model/title" />
          </XmlPropertyData>
        </ElementData>
      </XmlClassData>
      <XmlClassData TypeName="RequirementGroup" MonikerAttributeName="name" SerializeId="true" MonikerElementName="requirementGroupMoniker" ElementName="requirementGroup" MonikerTypeName="RequirementGroupMoniker">
        <DomainClassMoniker Name="RequirementGroup" />
        <ElementData>
          <XmlPropertyData XmlName="name" IsMonikerKey="true">
            <DomainPropertyMoniker Name="RequirementGroup/name" />
          </XmlPropertyData>
          <XmlPropertyData XmlName="description">
            <DomainPropertyMoniker Name="RequirementGroup/description" />
          </XmlPropertyData>
          <XmlPropertyData XmlName="id">
            <DomainPropertyMoniker Name="RequirementGroup/id" />
          </XmlPropertyData>
          <XmlRelationshipData UseFullForm="true" RoleElementName="requirements">
            <DomainRelationshipMoniker Name="RequirementGroupHasRequirements" />
          </XmlRelationshipData>
        </ElementData>
      </XmlClassData>
      <XmlClassData TypeName="ModelHasGroups" MonikerAttributeName="" SerializeId="true" MonikerElementName="modelHasGroupsMoniker" ElementName="modelHasGroups" MonikerTypeName="ModelHasGroupsMoniker">
        <DomainRelationshipMoniker Name="ModelHasGroups" />
      </XmlClassData>
      <XmlClassData TypeName="requirementsDiagram" MonikerAttributeName="" SerializeId="true" MonikerElementName="requirementsDiagramMoniker" ElementName="requirementsDiagram" MonikerTypeName="RequirementsDiagramMoniker">
        <DiagramMoniker Name="RequirementsDiagram" />
      </XmlClassData>
      <XmlClassData TypeName="RequirementGroupShape" MonikerAttributeName="" SerializeId="true" MonikerElementName="requirementGroupShapeMoniker" ElementName="requirementGroupShape" MonikerTypeName="RequirementGroupShapeMoniker">
        <GeometryShapeMoniker Name="RequirementGroupShape" />
      </XmlClassData>
      <XmlClassData TypeName="RequirementGroupHasRequirements" MonikerAttributeName="" SerializeId="true" MonikerElementName="requirementGroupHasRequirementsMoniker" ElementName="requirementGroupHasRequirements" MonikerTypeName="RequirementGroupHasRequirementsMoniker">
        <DomainRelationshipMoniker Name="RequirementGroupHasRequirements" />
      </XmlClassData>
      <XmlClassData TypeName="Requirement" MonikerAttributeName="" SerializeId="true" MonikerElementName="requirementMoniker" ElementName="requirement" MonikerTypeName="RequirementMoniker">
        <DomainClassMoniker Name="Requirement" />
        <ElementData>
          <XmlRelationshipData UseFullForm="true" RoleElementName="comments">
            <DomainRelationshipMoniker Name="RequirementHasComments" />
          </XmlRelationshipData>
          <XmlRelationshipData UseFullForm="true" RoleElementName="version">
            <DomainRelationshipMoniker Name="RequirementHasVersion" />
          </XmlRelationshipData>
          <XmlPropertyData XmlName="title">
            <DomainPropertyMoniker Name="Requirement/title" />
          </XmlPropertyData>
          <XmlPropertyData XmlName="description">
            <DomainPropertyMoniker Name="Requirement/description" />
          </XmlPropertyData>
          <XmlPropertyData XmlName="id">
            <DomainPropertyMoniker Name="Requirement/id" />
          </XmlPropertyData>
        </ElementData>
      </XmlClassData>
      <XmlClassData TypeName="RequirementShape" MonikerAttributeName="" SerializeId="true" MonikerElementName="requirementShapeMoniker" ElementName="requirementShape" MonikerTypeName="RequirementShapeMoniker">
        <GeometryShapeMoniker Name="RequirementShape" />
      </XmlClassData>
      <XmlClassData TypeName="RequirementHasComments" MonikerAttributeName="" SerializeId="true" MonikerElementName="requirementHasCommentsMoniker" ElementName="requirementHasComments" MonikerTypeName="RequirementHasCommentsMoniker">
        <DomainRelationshipMoniker Name="RequirementHasComments" />
      </XmlClassData>
      <XmlClassData TypeName="Comment" MonikerAttributeName="" SerializeId="true" MonikerElementName="commentMoniker" ElementName="comment" MonikerTypeName="CommentMoniker">
        <DomainClassMoniker Name="Comment" />
        <ElementData>
          <XmlPropertyData XmlName="subject">
            <DomainPropertyMoniker Name="Comment/subject" />
          </XmlPropertyData>
          <XmlPropertyData XmlName="body">
            <DomainPropertyMoniker Name="Comment/body" />
          </XmlPropertyData>
          <XmlPropertyData XmlName="author">
            <DomainPropertyMoniker Name="Comment/author" />
          </XmlPropertyData>
          <XmlPropertyData XmlName="created">
            <DomainPropertyMoniker Name="Comment/created" />
          </XmlPropertyData>
        </ElementData>
      </XmlClassData>
      <XmlClassData TypeName="RequirementHasVersion" MonikerAttributeName="" SerializeId="true" MonikerElementName="requirementHasVersionMoniker" ElementName="requirementHasVersion" MonikerTypeName="RequirementHasVersionMoniker">
        <DomainRelationshipMoniker Name="RequirementHasVersion" />
      </XmlClassData>
      <XmlClassData TypeName="Version" MonikerAttributeName="" SerializeId="true" MonikerElementName="versionMoniker" ElementName="version" MonikerTypeName="VersionMoniker">
        <DomainClassMoniker Name="Version" />
        <ElementData>
          <XmlPropertyData XmlName="major">
            <DomainPropertyMoniker Name="Version/major" />
          </XmlPropertyData>
          <XmlPropertyData XmlName="minor">
            <DomainPropertyMoniker Name="Version/minor" />
          </XmlPropertyData>
          <XmlPropertyData XmlName="service">
            <DomainPropertyMoniker Name="Version/service" />
          </XmlPropertyData>
        </ElementData>
      </XmlClassData>
      <XmlClassData TypeName="CommentShape" MonikerAttributeName="" SerializeId="true" MonikerElementName="commentShapeMoniker" ElementName="commentShape" MonikerTypeName="CommentShapeMoniker">
        <GeometryShapeMoniker Name="CommentShape" />
      </XmlClassData>
      <XmlClassData TypeName="VersionShape" MonikerAttributeName="" SerializeId="true" MonikerElementName="versionShapeMoniker" ElementName="versionShape" MonikerTypeName="VersionShapeMoniker">
        <GeometryShapeMoniker Name="VersionShape" />
      </XmlClassData>
      <XmlClassData TypeName="Connector" MonikerAttributeName="" SerializeId="true" MonikerElementName="connectorMoniker" ElementName="connector" MonikerTypeName="ConnectorMoniker">
        <ConnectorMoniker Name="Connector" />
      </XmlClassData>
    </ClassData>
  </XmlSerializationBehavior>
  <ExplorerBehavior Name="requirementsExplorer" />
  <Diagram Id="3b3c223a-5b84-4b03-97a0-e8b2b5dbf170" Description="Description for edom.requirements.RequirementsDiagram" Name="RequirementsDiagram" DisplayName="Minimal Language Diagram" Namespace="edom.requirements">
    <Class>
      <DomainClassMoniker Name="Model" />
    </Class>
    <ShapeMaps>
      <ShapeMap>
        <DomainClassMoniker Name="RequirementGroup" />
        <ParentElementPath>
          <DomainPath>ModelHasGroups.Model/!Model</DomainPath>
        </ParentElementPath>
        <DecoratorMap>
          <TextDecoratorMoniker Name="RequirementGroupShape/name" />
          <PropertyDisplayed>
            <PropertyPath>
              <DomainPropertyMoniker Name="RequirementGroup/name" />
              <DomainPath />
            </PropertyPath>
          </PropertyDisplayed>
        </DecoratorMap>
        <DecoratorMap>
          <TextDecoratorMoniker Name="RequirementShape/title" />
          <PropertyDisplayed>
            <PropertyPath>
              <DomainPropertyMoniker Name="RequirementGroup/name" />
            </PropertyPath>
          </PropertyDisplayed>
        </DecoratorMap>
        <GeometryShapeMoniker Name="RequirementGroupShape" />
      </ShapeMap>
      <ShapeMap>
        <DomainClassMoniker Name="Requirement" />
        <ParentElementPath>
          <DomainPath>RequirementGroupHasRequirements.RequirementGroup/!RequirementGroup/ModelHasGroups.Model/!Model</DomainPath>
        </ParentElementPath>
        <DecoratorMap>
          <TextDecoratorMoniker Name="RequirementShape/title" />
          <PropertyDisplayed>
            <PropertyPath>
              <DomainPropertyMoniker Name="Requirement/title" />
            </PropertyPath>
          </PropertyDisplayed>
        </DecoratorMap>
        <GeometryShapeMoniker Name="RequirementShape" />
      </ShapeMap>
      <ShapeMap>
        <DomainClassMoniker Name="Comment" />
        <ParentElementPath>
          <DomainPath>RequirementHasComments.Requirement/!Requirement/RequirementGroupHasRequirements.RequirementGroup/!RequirementGroup/ModelHasGroups.Model/!Model</DomainPath>
        </ParentElementPath>
        <DecoratorMap>
          <TextDecoratorMoniker Name="CommentShape/subject" />
          <PropertyDisplayed>
            <PropertyPath>
              <DomainPropertyMoniker Name="Comment/subject" />
            </PropertyPath>
          </PropertyDisplayed>
        </DecoratorMap>
        <GeometryShapeMoniker Name="CommentShape" />
      </ShapeMap>
      <ShapeMap>
        <DomainClassMoniker Name="Version" />
        <ParentElementPath>
          <DomainPath>RequirementHasVersion.Requirement/!Requirement/RequirementGroupHasRequirements.RequirementGroup/!RequirementGroup/ModelHasGroups.Model/!Model</DomainPath>
        </ParentElementPath>
        <DecoratorMap>
          <TextDecoratorMoniker Name="VersionShape/major" />
          <PropertyDisplayed>
            <PropertyPath>
              <DomainPropertyMoniker Name="Version/major" />
            </PropertyPath>
          </PropertyDisplayed>
        </DecoratorMap>
        <DecoratorMap>
          <TextDecoratorMoniker Name="VersionShape/minor" />
          <PropertyDisplayed>
            <PropertyPath>
              <DomainPropertyMoniker Name="Version/minor" />
            </PropertyPath>
          </PropertyDisplayed>
        </DecoratorMap>
        <GeometryShapeMoniker Name="VersionShape" />
      </ShapeMap>
    </ShapeMaps>
    <ConnectorMaps>
      <ConnectorMap>
        <ConnectorMoniker Name="Connector" />
        <DomainRelationshipMoniker Name="RequirementGroupHasRequirements" />
      </ConnectorMap>
      <ConnectorMap>
        <ConnectorMoniker Name="Connector" />
        <DomainRelationshipMoniker Name="RequirementHasComments" />
      </ConnectorMap>
      <ConnectorMap>
        <ConnectorMoniker Name="Connector" />
        <DomainRelationshipMoniker Name="RequirementHasVersion" />
      </ConnectorMap>
    </ConnectorMaps>
  </Diagram>
  <Designer CopyPasteGeneration="CopyPasteOnly" FileExtension="requirements" EditorGuid="48e2478c-4fb9-445b-a18b-9d321e37a41f">
    <RootClass>
      <DomainClassMoniker Name="Model" />
    </RootClass>
    <XmlSerializationDefinition CustomPostLoad="false">
      <XmlSerializationBehaviorMoniker Name="requirementsSerializationBehavior" />
    </XmlSerializationDefinition>
    <ToolboxTab TabText="requirements">
      <ElementTool Name="RequirementGroup" ToolboxIcon="resources\exampleshapetoolbitmap.bmp" Caption="RequirementGroup" Tooltip="Create an ExampleElement" HelpKeyword="CreateExampleClassF1Keyword">
        <DomainClassMoniker Name="RequirementGroup" />
      </ElementTool>
      <ElementTool Name="Requirement" ToolboxIcon="Resources\ExampleShapeToolBitmap.bmp" Caption="Requirement" Tooltip="Requirement" HelpKeyword="Requirement">
        <DomainClassMoniker Name="Requirement" />
      </ElementTool>
      <ElementTool Name="Comment" ToolboxIcon="Resources\ExampleShapeToolBitmap.bmp" Caption="Comment" Tooltip="Comment" HelpKeyword="Comment">
        <DomainClassMoniker Name="Comment" />
      </ElementTool>
      <ElementTool Name="Version" ToolboxIcon="Resources\ExampleShapeToolBitmap.bmp" Caption="Version" Tooltip="Version" HelpKeyword="Version">
        <DomainClassMoniker Name="Version" />
      </ElementTool>
    </ToolboxTab>
    <Validation UsesMenu="true" UsesOpen="true" UsesSave="true" UsesCustom="true" UsesLoad="true" />
    <DiagramMoniker Name="RequirementsDiagram" />
  </Designer>
  <Explorer ExplorerGuid="d058f462-44b7-4aa9-9f54-0f03df4891ee" Title="requirements Explorer">
    <ExplorerBehaviorMoniker Name="requirements/requirementsExplorer" />
  </Explorer>
</Dsl>