Topic: MODEL VALIDATION
Approach: Validation using Epsilon

By its nature a model is more abstract than the system it represents (even a simulation model). Viewed in one way, abstraction, and assumptions we make to achieve it, eliminate unnecessary detail and allow us to focus on the elements within the system which are important from a performance point of view; viewed in another way, this abstraction process introduces inaccuracy. Some degree of inaccuracy may be necessary, desirable even, to make the model solution tractable and/or efficient. Inevitably some assumptions must be made about the system in order to construct the model. However, having made such assumptions we must expect to put some effort into answering questions about the goodness of our model. There are two steps to judging how good a model is with respect to the system. We must ascertain whether the model implements the assumptions correctly (model verification) and whether the assumptions which have been made are reasonable with respect to the real system (model validation).


1. Model Validation

Validation is the task of demonstrating that the model is a reasonable representation of the actual system: that it reproduces system behaviour with enough fidelity to satisfy analysis objectives. Whereas model verification techniques are general the approach taken to model validation is likely to be much more specific to the model, and system, in question. Indeed, just as model development will be influenced by the objectives of the performance study, so will model validation be. 

A model is usually developed to analyse a particular problem and may therefore represent different parts of the system at different levels of abstraction. As a result, the model may have different levels of validity for different parts of the system across the full spectrum of system behaviour. For most models there are three separate aspects which should be considered during model validation:
• assumptions
• input parameter values and distributions
• output values and conclusions.

However, in practice it may be difficult to achieve such a full validation of the model, especially if the system being modelled does not yet exist. In general, initial validation attempts will concentrate on the output of the model, and only if that validation suggests a problem will more detailed validation be undertaken.
Broadly speaking there are three approaches to model validation and any combination of them may be applied as appropriate to the different aspects of a particular model. These approaches are:
• expert intuition
• real system measurements
• theoretical results/analysis.

In addition, as suggested above, ad hoc validation techniques may be established for a particular model and system.


2. Model Validation in Epsilon

Epsilon is an Eclipse project that provides languages for model management tasks such as validation, transformation, comparison and pattern matching. All task-specific languages build upon a feature-rich model-oriented language – the Epsilon Object Language (EOL) – a dynamically typed, interpreted language which supports imperative programming. EOL also supports native types, effectively allowing for execution of arbitrary Java code. A key feature of Epsilon is that it works across a range of modelling technologies by abstracting model operations through a connectivity layer, so a script written to work on an EMF model can also be used on an XML document or a spreadsheet without changes.

Epsilon Validation Language (EVL) is a validation language built on top of EOL. In their simplest form, EVL constraints are quite similar to OCL constraints. However, EVL also supports dependencies between constraints (e.g. if constraint A fails, don't evaluate constraint B), customizable error messages to be displayed to the user and specification of fixes (in EOL) which users can invoke to repair inconsistencies. Also, as EVL builds on EOL, it can evaluate inter-model constraints (unlike OCL). The EVL extends EOL to enable users to express their validation constraints in a more structured and declarative manner. 

Since EOL supports all features of OCL with similar syntax and built-in
operations, EVL is can be used in the same manner as OCL. Users define constraints within the context of a model element type, where each constraint has a check expression (or statement block for more complex constraints) returning a Boolean. In addition, constraints may also have a guard, which is semantically identical to prepending the constraint check expression with a Boolean expression followed by the implies operator. Guard blocks can also be declared in a context. 

Constraints may have dependencies on other constraints using the satisfies operation, which returns the result of calling the specified constraint(s) for the current element. Constraints declared as lazy will only be executed when invoked by a satisfies operation. A context declared as lazy is equivalent to having all of its constraints being lazy. EVL also allows users to defined fixes for constraints, which can modify the model with arbitrary imperative code. Like all Epsilon rule-based languages, EVL has pre and post blocks, which allow for
arbitrary code to be run before and after the main program, respectively.

3. Model validation with Java program using Epsilon

Suppose that we have a model of a Java program, and we want to ensure that every class overrides the equals method according to the contract (Equal objects must have the same hash code, but unequal objects do not necessarily have different hash codes). The code listing bellow (EVL program over Java metamodel) shows how this could be implemented in EVL. Note that by declaring the hasHashCode constraint as lazy, we only check it once per ClassDeclaration as a pre-condition for the hasEquals. If hasHashCode fails for the current element under consideration (referred to as self ), then we avoid executing the hasEquals check expression for the current class. This means a class may fail to satisfy either hasHashCode or hasEquals, but not both. Therefore our results will never contain the same class more than once. Also note that by declaring the getMethods operation as cached, we avoid re-evaluating it for a given model element, so that if a class satisfies hasHashCode, we do not need to find all of its methods again in line 9.

1 @cached
2 operation AbstractTypeDeclaration getMethods() : Collection {
3   return self.bodyDeclarations.select(bd|bd.isKindOf(MethodDeclaration);
4 }
5
6 context ClassDeclaration {
7   constraint hasEquals {
8       guard : self.satisfies("hasHashCode")
9       check : self.getMethods().exists(method |
10      method.name == "equals" and
11      method.parameters.size() == 1 and
12      method.parameters.first().type.type.name == "Object" and
13      method.modifier.isDefined() and
14      method.modifier.visibility == VisibilityKind#public and
15      method.returnType.type.isTypeOf(PrimitiveTypeBoolean))
16  }
17  @lazy
18  constraint hasHashCode {
19      check : self.getMethods().exists(method |
20      method.name == "hashCode" and
21      method.parameters.isEmpty() and
22      method.modifier.isDefined() and
23      method.modifier.visibility == VisibilityKind#public and
24      method.returnType.type.isTypeOf(PrimitiveTypeInt))
25  }
26 }

The execution algorithm for sequential EVL is given in the listing bellow (Simplified sequential EVL algorithm).

1 preBlock.execute();
2 for (Context context : contexts) {
3    for (Object element : context.getAllOfKind()) {
4       if (!context.isLazy() && context.guard(element)) {
5           for (Constraint constraint : context.getConstraints()) {
6               if (!constraint.isLazy() && constraint.guard(element)) {
7                   if (!constraint.check(element)) {
8                       unsatisfiedConstraints.add(constraint, element);
9                   }
10              }
11          }
12      }
13   }
14 }
15 postBlock.execute();


For each context (line 2), we loop through all elements of that type and subtypes (line 3). Provided that the guard blocks of each context and constraint are satisfied and they are not marked as lazily evaluated (lines 4 and 6 respectively), we simply execute the check block (line 7) of each constraint within the declared context (line 5) for the current element. We add each failure to the set of unsatisfied constraints (line 8). Not shown in Listing 2 is the constraint trace, which keeps track of results to avoid re-evaluating constraint and element pairs in case of a satisfies operation (i.e. dependencies between constraints). The semantics of how this is used will be discussed in the next section. Also note that the pre and post blocks (lines 1 and 15, respectively are not of interest as they may contain arbitrary imperative code. We have also excluded fixes for simplicity.

4. OCL vs EVL

EVL Features:

- Distinguish between errors and warnings during validation (constraints and critiques);
- Specify quick fixes for failed constraints;
- Guarded constraints;
- Specify constraint dependencies;
- Break down complex constraints to sequences of simpler statements;
- Automated constraint evaluation;
- Out-of-the-box integration with the EMF validation framework and GMF;
- Support for parallel execution;
- Support for simultaneously accessing/modifying many models of (potentially) different metamodels;
- All the usual programming constructs (while and for loops, statement sequencing, variables etc.);
- Support for those convenient first-order logic OCL operations (select, reject, collect etc.);
- Ability to create and call methods of Java objects;
- Support for dynamically attaching operations to existing meta-classes and types at runtime;
- Support for cached operations;
- Support for extended properties;
- Support for user interaction;
- Ability to create reusable libraries of operations and import them from different Epsilon (not only EOL) modules;

OCL Features:

Among the many applications of OCL, it can be used to define the following kinds of expressions (for the sake of simplicity we focus on OCL usages in class diagrams):

- Invariants  to state all  necessary  condition  that must  be  satisfied  in  each possible instantiation of the model.
- Initialization of class properties.
- Derivation rules that express how the value of derived model elements must be computed.
- Query  operations
- Operation contracts (i.e., set of operation pre- and postconditions)

5. Small review of litterature references

Paige, R.F., Kolovos, D.S., Rose, L.M., Drivalos, N., Polack, F.A.C.: The Design of a Conceptual Framework and Technical Infrastructure for Model Management Language Engineering, pp. 162–171 (2009);

Kolovos, D. S., Paige, R.F., Polack, F.A.C.: The Epsilon Object Language (EOL), Bilbao. pp. 128–142 (2006);

Madani S., Kolovos D. S., Paige R. F., Parallel Model Validation with Epsilon, Department of Computer Science, University of York, pp. 2-5 (2017);

Epsilon Validation Language - Model Constraint/Validation Language. (2018). Eclipse.org. Retrieved 2 December 2018, from https://www.eclipse.org/epsilon/doc/evl/
